<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubsidioAsistencia extends Model
{
    //
    protected $table =  "subsidio_asistencia";
    public $timestamps = false;
}
