<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Asistencia;
use App\Persona;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $personas = DB::table('personas')
        ->selectRaw('count(*) as total')
        ->get();

        $transportes = DB::table('personas')->selectRaw('count(*) as total')
        ->whereRaw('transporte = 1')->get();

        $asistencias = Asistencia::where('fecha', '=',  date('Y-m-d'))->get();

        $subsidios = DB::table('subsidios')
        ->selectRaw('sum(monto_total) as total')
        ->get();

        return view('layouts.principal')->with(compact('personas','transportes', 'asistencias', 'subsidios'));
    }
}
