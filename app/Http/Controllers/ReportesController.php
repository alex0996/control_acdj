<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\SubsidioAsistencia;
use App\Persona;
class ReportesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('control.reportes');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request ,$id)
    {
        //
        if($id == "personas"){
            $option = $id;
        }
        else{
            $option = $request->get('tipo');
        }
        
        switch ($option) {
            case "list-asistMes-list":
                
                $datos = ['fecha' => ' '.$request->get('mes').'-'.$request->get('fechaMes').''];
                $personas = DB::select('call reporteAsistenciaMes('.$request->get('mes').','.$request->get('fechaMes').')'); 
                return view('reportes.reporteAsistencia')->with(compact('datos','personas'));
                break;
            
            case "list-asistRango-list":
                $datos = ['fecha' => 'de '.$request->get('fecha1').' hasta '.$request->get('fecha2').''];
                $personas = DB::select('call reporteAsistenciaRango("'.$request->get('fecha1').'","'.$request->get('fecha2').'")'); 
                return view('reportes.reporteAsistencia')->with(compact('datos','personas'));
                break;

            case "list-subMes-list":
                $datos = ['fecha' => ' '.$request->get('mes').'-'.$request->get('fechaMes').''];
                $subsidioAsistencias = SubsidioAsistencia::whereRaw('MONTH(fecha) = ?',$request->get('mes'))->whereRaw('YEAR(fecha) = ?',$request->get('fechaMes'))->get();
                return view('reportes.reporteSubsidio')->with(compact('datos','subsidioAsistencias'));

            case "list-subRango-list":
                $datos = ['fecha' => 'de '.$request->get('fecha1').' hasta '.$request->get('fecha2').''];
                $subsidioAsistencias = SubsidioAsistencia::whereBetween('fecha', array($request->get('fecha1'),$request->get('fecha2')))->get();
                return view('reportes.reporteSubsidio')->with(compact('datos','subsidioAsistencias'));
                break; 

            case "list-subTotalMes-list":
                
                $datos = ['fecha' => ' '.$request->get('mes').'-'.$request->get('fechaMes').''];
                $subsidios = DB::select('call reporteSubsidiosMes('.$request->get('mes').','.$request->get('fechaMes').')'); 
                return view('reportes.reporteSubsidioUtilizado')->with(compact('datos','subsidios'));
                break;
            
            case "list-subTotalRango-list":
                $datos = ['fecha' => 'de '.$request->get('fecha1').' hasta '.$request->get('fecha2').''];
                $subsidios = DB::select('call reporteSubsidiosRango("'.$request->get('fecha1').'","'.$request->get('fecha2').'")'); 
                return view('reportes.reporteSubsidioUtilizado')->with(compact('datos','subsidios'));
                break;
            
            case "list-subTransMes-list":
                
                $datos = ['fecha' => ' '.$request->get('mes').'-'.$request->get('fechaMes').''];
                $personas = DB::select('call reporteTransporteMes('.$request->get('mes').','.$request->get('fechaMes').')'); 
                return view('reportes.reporteUsoTransporte')->with(compact('datos','personas'));
                break;
                
            case "list-subTransRango-list":
                $datos = ['fecha' => 'de '.$request->get('fecha1').' hasta '.$request->get('fecha2').''];
                $personas = DB::select('call reporteTransporteRango("'.$request->get('fecha1').'","'.$request->get('fecha2').'")'); 
                return view('reportes.reporteUsoTransporte')->with(compact('datos','personas'));
                break;

            case "personas":
                $datos = ['fecha' => date('Y-m-d')];
                $personas = Persona::all(); 
                return view('reportes.reportePersonas')->with(compact('datos','personas'));
                break;
    

            default:
               return "No es por mes";
        }
   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
