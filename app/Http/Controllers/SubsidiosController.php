<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subsidio;

class SubsidiosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $subsidios = Subsidio::all();
        return view('mantenimientos.gestionSubsidios')->with('subsidios',$subsidios); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $subsidio = new Subsidio;
        $subsidio->entidad = $request->get('entidad');
        $subsidio->monto_total = $request->get('monto_total');
        $subsidio->cantidad_usuarios = $request->get('cantidad_usuarios');
        $subsidio->monto_usuario =  ($request->get('monto_total') / $request->get('cantidad_usuarios')) / cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
        $subsidio->save();
        return '{"msg": "success"}';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $subsidio = Subsidio::find($id);
        return $subsidio;
    }

    public static function showFromSubsidios($id)
    {
        //
        $subsidio = Subsidio::find($id);
        return $subsidio;
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $subsidio = Subsidio::find($id);
        $subsidio->entidad = $request->get('entidad');
        $subsidio->monto_total = $request->get('monto_total');
        $subsidio->cantidad_usuarios = $request->get('cantidad_usuarios');
        $subsidio->monto_usuario = ($request->get('monto_total') / $request->get('cantidad_usuarios')) / cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
        $subsidio->save();
        return '{"msg": "success"}';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $subsidio = Subsidio::find($id);
        $subsidio->delete();
        return '{"msg": "success"}';
    }
}
