<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Persona;
use App\Pension;
class PersonasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
        $personas = Persona::all();
        $pensiones = Pension::all();
        //$causas = Causas::orderBy('id','desc')->paginate(3);
        return view('mantenimientos.gestionPersonas')->with(compact('personas','pensiones'));
          
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $persona = new Persona;

        $persona->id = $request->get('id');
        $persona->cedula = $request->get('cedula');
        $persona->nombre = $request->get('nombre');
        $persona->fecha_nacimiento = $request->get('fecha_nacimiento');
        $persona->sexo = $request->get('sexo');
        $persona->tipo_pension = $request->get('tipo_pension');
        $persona->monto_pension = $request->get('monto_pension');
        $persona->transporte = $request->get('transporte');
        $persona->donacion = $request->get('donacion');
        $persona->save();
        return '{"msg": "success"}';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $persona = Persona::find($id);
        return $persona;
    }
    
    public static function showFromAssistance($id)
    {
        //
        $persona = Persona::find($id);
        return $persona;
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $persona = Persona::find($id);
        $persona->cedula = $request->get('cedula');
        $persona->nombre = $request->get('nombre');
        $persona->fecha_nacimiento = $request->get('fecha_nacimiento');
        $persona->sexo = $request->get('sexo');
        $persona->tipo_pension = $request->get('tipo_pension');
        $persona->monto_pension = $request->get('monto_pension');
        $persona->transporte = $request->get('transporte');
        $persona->donacion = $request->get('donacion');
        $persona->save();
        
        return '{"msg": "success"}';


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $persona = Persona::find($id);
        $persona->delete();
        
        return '{"msg": "success"}';
    }
}
