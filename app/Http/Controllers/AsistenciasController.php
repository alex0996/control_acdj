<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Asistencia;
use App\Causa;
use App\Persona;
use App\SubsidioAsistencia;
use App\Subsidio;

class AsistenciasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sesion = Asistencia::where('fecha', '=',  date('Y-m-d'))->get();
       

        if (!count($sesion) > 0 ){
            //inicializa lista de asistencia
            $personas =  Persona::all();
            foreach ($personas as $persona){
                    $asistencia = new Asistencia;
                    $asistencia->id = $persona->id;
                    $asistencia->fecha = date('Y-m-d');
                    $asistencia->asistio = 1;
                    $asistencia->causa = 5;
                    $asistencia->save();
            }

            $subsidios = Subsidio::all();
            if (count($subsidios) > 0 ){
            foreach ($subsidios as $subsidio){
                $subAsistencia = new SubsidioAsistencia;
                $subAsistencia->id_subsidio = $subsidio->id;
                $subAsistencia->monto_total = $subsidio->monto_total;
                $subAsistencia->cantidad_usuarios = $subsidio->cantidad_usuarios;
                $subAsistencia->monto_usuario = $subsidio->monto_usuario;
                $subAsistencia->fecha = date('Y-m-d');
                $subAsistencia->save();
                }
            }
        }
        
        
        
        $asistencias = Asistencia::where('fecha', '=',  date('Y-m-d'))->where('asistio','=',1)->get();
        $causas = Causa::all();
       return view('control.asistencias')->with(compact('asistencias','causas'));
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        //
        $fecha = $request->get('fecha');    
        $asistencias = Asistencia::where('fecha', '=', $fecha)->where('id','=',$id)->get();
        return $asistencias;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        Asistencia::where('fecha', '=',  date('Y-m-d'))
        ->where('id','=',$id)
        ->update(['asistio' => 0, 'causa' => $request->get('causa')]);
        
        return '{"msg": "success"}';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
