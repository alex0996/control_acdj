<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pension;

class PensionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $pensiones = Pension::all();
        return view('mantenimientos.gestionPensiones')->with('pensiones',$pensiones);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $pension = new Pension;

        $pension->id = $request->get('id');
        $pension->descripcion = $request->get('descripcion');
        $pension->save();
        return '{"msg": "success"}';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $tipo = Pension::find($id);
        return $tipo;
    }

    public static function showFromPeople($id)
    {
        //
        $tipo = Pension::find($id);
        return $tipo;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $pension = Pension::find($id);
        $pension->descripcion = $request->get('descripcion');
        $pension->save();
        return '{"msg": "success"}';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $pension = Pension::find($id);
        $pension->delete();
        return '{"msg": "success"}';
    }
}
