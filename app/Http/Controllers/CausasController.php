<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Causa;

class CausasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $causas = Causa::all();
        return view('mantenimientos.gestionCausas')->with('causas',$causas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $causa = new Causa;
        $causa->id = $request->get('id');
        $causa->motivo = $request->get('motivo');
        $causa->save();
        return '{"msg": "success"}';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $causa = Causa::find($id);
        return $causa;
    }

    public static function showFromAssistance($id){
        //
        $causa = Causa::find($id);
        return $causa;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $causa = Causa::find($id);
        $causa->motivo = $request->get('motivo');
        $causa->save();
        return '{"msg": "success"}';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $causa = Causa::find($id);
        $causa->delete();
        return '{"msg": "success"}';
    }
}
