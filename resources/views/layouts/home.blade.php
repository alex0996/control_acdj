<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
     <link rel="icon" href="{{ asset('img/logo.png') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>
        Control de Asistencia ACDJ
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"/>
    
    <!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.18/b-1.5.4/b-flash-1.5.4/datatables.min.css"/>-->
 
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.18/b-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fc-3.2.5/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.css"/>
 
    

    <!-- CSS Files -->
    <link href="{{ asset('css/material-dashboard.min.css?v=2.1.0') }}" rel="stylesheet"/>
    <link href="{{ asset('css/dashboard.css') }}" rel="stylesheet"/>
    
    <!-- JS Files 
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.18/b-1.5.4/b-flash-1.5.4/datatables.min.js"></script>
    -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.18/b-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fc-3.2.5/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{ asset('js/core/popper.min.js') }}" defer></script>
  <script src="{{ asset('js/core/bootstrap-material-design.min.js') }}" defer></script>
  <script src="{{ asset('js/plugins/perfect-scrollbar.jquery.min.js') }}" defer></script>
  <script src="{{ asset('js/metodos/li.js') }}" defer></script>
  <script src="{{ asset('js/metodos/convertirJson.js') }}" defer></script>


</head>
<body>
<div class="wrapper ">
    <div class="sidebar" data-color="azure" data-background-color="white" data-image="{{asset('img/sidebar-1.jpg')}}">
        <div class="logo">
            <img src="{{ asset('img/img.jpg') }}" class="pl-3" width="250" alt="">
            <label class="simple-text logo-normal">
                Control de Asistencia
            </label>

            
        </div>
        <div class="sidebar-wrapper">
            <ul class="nav">
                
               <li class="nav-item " id="home">
                            <a class="nav-link" href="/home">
                                <i class="material-icons">home</i>
                                <p>Inicio</p>
                            </a>
                </li>
                <li class="nav-item " id="asistencias">
                    <a class="nav-link" href="/asistencias">
                        <i class="material-icons">assignment_turned_in</i>
                        <p>Asistencia</p>
                    </a>
                </li>
                <li class="nav-item " id="reportes">
                        <a class="nav-link" href="/reportes">
                            <i class="material-icons">library_books</i>
                            <p>Reportes</p>
                        </a>
                    </li>
                <li class="nav-item " id="buscarAsistencia">
                    <a class="nav-link" href="/buscarAsistencia">
                        <i class="material-icons">assignment_ind</i>
                        <p>Gestión de asistencias</p>
                    </a>
                 </li>
                <li class="nav-item"  id="personas">
                    <a class="nav-link" href="/personas">
                        <i class="material-icons">group</i>
                        <p>Gestión de personas</p>
                    </a>
                </li>
                <li class="nav-item "  id="pensiones">
                        <a class="nav-link" href="/pensiones">
                            <i class="material-icons">business_center</i>
                            <p>Gestión de pensiones</p>
                        </a>
                </li>
                <li class="nav-item " id="subsidios">
                        <a class="nav-link" href="/subsidios" >
                            <i class="material-icons">account_balance</i>
                            <p>Gestión de subsidios</p>
                        </a>
                </li>
                <li class="nav-item " id="causas">
                        <a class="nav-link" href="/causas">
                            <i class="material-icons">do_not_disturb_on</i>
                            <p>Gestión de causas</p>
                        </a>
                </li>
                
            </ul>
        </div>
    </div>
    <div class="main-panel">
        <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
            <div class="container-fluid">

                <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                                    <a class="btn btn-white btn-round btn-just-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                     <i class="material-icons">more_vert</i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                         <a class="dropdown-item"> <i class="material-icons pr-2">person</i>  {{ Auth::user()->name }} </a>
                                        <a class="dropdown-item" href="{{ route('register') }}">Registrar Usuario</a>
                                        <a class="dropdown-item" href="#" href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                              document.getElementById('logout-form').submit();">
                                            Salir    
                                            </a>
                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                        @csrf
                                                    </form>

                                         <a href="#" data-toggle="modal" data-target="#aboutModal" class="dropdown-item">Acerca de</a>
                                      </div>     
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        
        
        <div class="content">
            <!--Aca va el contenido de cada pagina -->
            @yield('content')
        </div>

        <footer class="footer">
            <div class="container-fluid">
                <nav class="float-left">
                    <ul>
                        <li>
                            <a href="https://www.facebook.com/CentroDiurnoJerusalenCR/" a target="_blank" >
                                <i class="fa fa-facebook"></i>  Asociación Centro Diurno Jerusalén
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </footer>
    </div>
</div>


<!-- about Modal -->
<div class="modal fade" id="aboutModal" tabindex="-1" role="dialog" aria-labelledby="aboutModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="aboutModal">Sistema desarrollado por: </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="row">
          <ul class="col-md-6 offset-md-1 col align-self-start">
            <li>Alex José Baltodano Paniagua</li>
            <li>Caleb Pérez Granados</li>
            <li>José Carlos Barrantes Araya</li>
            <li>Greivin Barrantes Segura</li>
            <li>Andrés Barrantes Murillo</li>
          </ul>
          <div class="col-md-3 offset-md-1 col align-self-end">
                <img src="{{ asset('img/una.png')}}" alt="" style="width: 100px;">
          </div>
          
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>


</body>
</html>