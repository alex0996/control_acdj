@extends('layouts.home')

@section('content')

<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
                <div class="row">
                        <div class="col-md-6">
                                <h4 class="card-title ">Información</h4>
                                
                        </div>  
                        <div class="col-md-1 offset-md-5">
                        
                        </div>
                    </div>
            
        </div>
        <div class="card-body  mt-2">
        

                <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                          <div class="card card-stats">
                            <div class="card-header card-header-info card-header-icon">
                              <div class="card-icon">
                                <i class="material-icons">person</i>
                              </div>
                              <p class="card-category">Total de personas</p>
                              <h3 class="card-title">@if (count($personas) > 0)
                                    @foreach ($personas as $persona)
                                    {{$persona->total}}
                                    @endforeach
                                    @endif
                              </h3>
                            </div>
                            <div class="card-footer">
                              <div class="stats">
                               </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                          <div class="card card-stats">
                            <div class="card-header card-header-secondary card-header-icon">
                              <div class="card-icon">
                                <i class="material-icons">directions_bus</i>
                              </div>
                              <p class="card-category">Personas con transporte</p>
                              <h3 class="card-title">
                                    @if (count($transportes) > 0)
                                    @foreach ($transportes as $transporte)
                                    {{$transporte->total}}
                                    @endforeach
                                    @endif
                              </h3>
                            </div>
                            <div class="card-footer">
                              <div class="stats">
                              </div>
                            </div>
                          </div>
                        </div>
                        @if (count($asistencias) > 0)
                        <div class="col-lg-6 col-md-6 col-sm-6">
                          <div class="card card-stats">
                            <div class="card-header card-header-success card-header-icon">
                              <div class="card-icon">
                                <i class="material-icons">check</i>
                              </div>
                              <p class="card-category">Estado de asistencia</p>
                              <h3 class="card-title">Activa</h3>
                            </div>
                            <div class="card-footer">
                              <div class="stats">
                              </div>
                            </div>
                          </div>
                        </div>
                        @else
                        <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="card card-stats">
                                  <div class="card-header card-header-danger card-header-icon">
                                    <div class="card-icon">
                                      <i class="material-icons">info_outline</i>
                                    </div>
                                    <p class="card-category">Estado de asistencia</p>
                                    <h3 class="card-title">Inactiva</h3>
                                  </div>
                                  <div class="card-footer">
                                    <div class="stats">
                                    </div>
                                  </div>
                                </div>
                              </div>
                        @endif
                        <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="card card-stats">
                                  <div class="card-header card-header-success card-header-icon">
                                    <div class="card-icon">
                                      <i class="material-icons">account_balance</i>
                                    </div>
                                    <p class="card-category">Monto de subsidios</p>
                                    <h3 class="card-title">
                                            @if (count($subsidios) > 0)
                                            @foreach ($subsidios as $subsidio)
                                            ₡ {{$subsidio->total}}
                                            @endforeach
                                            @endif
                                    </h3>
                                  </div>
                                  <div class="card-footer">
                                    <div class="stats">
                                    </div>
                                  </div>
                                </div>
                        </div>
                      </div>

        </div>
    </div>
</div>


@endsection

