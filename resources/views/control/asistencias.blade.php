@extends('layouts.home')

@section('content')


<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
          
            <h4 class="card-title">Asistencia</h4>
            <p class="card-category">Fecha:  {{ date('Y-m-d') }}</p>
        </div>
        <div class="card-body mt-2">
        
            <div class="table-responsive">
                <table class="table" id="control_asistencias">
                    <thead class=" text-primary">
                    <th>
                        #
                    </th>
                    <th>
                        Cédula
                    </th>
                    <th>
                        Nombre
                    </th>
                    <th>
                        Causa
                    </th>
                    <th>
                        Marcar ausente
                    </th>            
                    </thead>
                    <tbody>
                            @if (count($asistencias) > 0)
                            @foreach ($asistencias as $asistencia)
                            <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{\App\Http\Controllers\PersonasController::showFromAssistance($asistencia->id)->cedula}}</td>
                                    <td>{{\App\Http\Controllers\PersonasController::showFromAssistance($asistencia->id)->nombre}}</td>
                                    <td><select class="form-control" name="{{$asistencia->id}}">
                                            <option value="" disabled selected>Click para ver opciones</option>
                                            @foreach ($causas as $causa)
                                            <option id="{{$causa->id}}" value="{{$causa->id}}">{{$causa->motivo}}</option>
                                            @endforeach
                                    </select>
                                    </td>
                                  <td><button type="button" rel="tooltip" title="Deshabilitar usuario" class="btn btn-danger btn-link absent" name="{{$asistencia->id}}">
                                        <i class="material-icons">event_busy</i>
                                </button></td>
                                    
                            </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">


            </div>
        </div>
    </div>
</div>



{{-- script de asistencias --}}
<script src="{{ asset('js/control/control_asistencias.js') }}" defer></script>

@endsection