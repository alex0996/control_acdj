@extends('layouts.home')

@section('content')

<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
                                <h4 class="card-title ">Reportes</h4>
                                <p class="card-category">Tipos de reportes</p>
                
        </div>
        <div class="card-body  mt-2">
 
            <div class="row">
                    
                    <div class="col-6">
                      <div class="list-group" id="list-tab" role="tablist">
                        <a class="list-group-item list-group-item-action active" id="list-asistMes-list" data-toggle="list" href="#list-divRepMes" role="tab" aria-controls="asistMes">Reporte de asistencia por mes</a>
                        <a class="list-group-item list-group-item-action" id="list-asistRango-list" data-toggle="list" href="#list-divRepRango" role="tab" aria-controls="asistRango">Reporte de asistencia por rango de fecha</a>
                        <a class="list-group-item list-group-item-action" id="list-subMes-list" data-toggle="list" href="#list-divRepMes" role="tab" aria-controls="subMes">Reporte de subsidios por mes</a>
                        <a class="list-group-item list-group-item-action" id="list-subRango-list" data-toggle="list" href="#list-divRepRango" role="tab" aria-controls="subRango">Reporte de subsidios por rango de fecha</a>
                        <a class="list-group-item list-group-item-action" id="list-subTotalMes-list" data-toggle="list" href="#list-divRepMes" role="tab" aria-controls="subMes">Reporte de subsidios utilizados por mes</a>
                        <a class="list-group-item list-group-item-action" id="list-subTotalRango-list" data-toggle="list" href="#list-divRepRango" role="tab" aria-controls="subRango">Reporte de subsidios utilizados por rango de fecha</a> 
                        <a class="list-group-item list-group-item-action" id="list-subTransMes-list" data-toggle="list" href="#list-divRepMes" role="tab" aria-controls="subMes">Reporte de uso de transporte por mes</a>
                        <a class="list-group-item list-group-item-action" id="list-subTransRango-list" data-toggle="list" href="#list-divRepRango" role="tab" aria-controls="subRango">Reporte de uso de transporte por rango de fecha</a> 
                        <a class="list-group-item list-group-item-action" href="/reportes/personas" >Reporte de personas</a> 
                         
                    </div>
                    </div>

                    <div class="col-6">
                        
                      <div class="tab-content" id="nav-tabContent">
                         
                            {{-- Form para reportes por mes --}}
                            <div class="tab-pane fade show active" id="list-divRepMes" role="tabpanel">
                                        <form action="/reportes/1">
                                        <div class="form-group">
                                                    <label for="mes">Mes</label>
                                                    <select class="form-control" name="mes" id="mes" required>
                                                      <option value="" disabled selected>Click para mostrar opciones</option>
                                                      <option value="01">Enero</option>
                                                      <option value="02">Febrero</option>
                                                      <option value="03">Marzo</option>
                                                      <option value="04">Abril</option>
                                                      <option value="05">Mayo</option>
                                                      <option value="06">Junio</option>
                                                      <option value="07">Julio</option>
                                                      <option value="08">Agosto</option>
                                                      <option value="09">Septiembre</option>
                                                      <option value="10">Octubre</option>
                                                      <option value="11">Noviembre</option>
                                                      <option value="12">Diciembre</option>
                                                    </select>
                                        </div>
                                        <div class="form-group">
                                                        <label for="fechaMes">Año</label>
                                                        <input type="number" class="form-control" name="fechaMes" id="fechaMes" value= {{ date('Y') }}>
                                        </div>
                                        
                                        <input type="text" class="form-control" name="tipo" value="list-asistMes-list" hidden>
                                        
                                    
                                    <input type="submit" class="btn btn-primary" value="GENERAR">
                                    
                                    </form>
                            </div>

                            {{-- Form para reportes por rango --}}
                            <div class="tab-pane fade" id="list-divRepRango" role="tabpanel">
                                    <form action="/reportes/1">
                                        <div class="form-group">
                                                        <label for="fecha1">Fecha de inicio</label>
                                                        <input type="date" class="form-control" name="fecha1" id="fecha1" required>
                                        </div>
                                        <div class="form-group">
                                                <label for="fecha2">Fecha final</label>
                                                <input type="date" class="form-control" name="fecha2" id="fecha2" required>
                                        </div>
                                        
                                        <input type="text" class="form-control" name="tipo" hidden>
                                    
                                    <input type="submit" class="btn btn-primary" value="GENERAR">
                                    
                                    </form>
                            </div>

                            {{-- Form para reportes por mes por persona --}}
                            {{-- Form para reportes por rango por persona --}}
                      
                        </div>

                    </div>
                  </div>
        
            
        </div>
    </div>
</div>


<script>

$('a').click(function () { 
    
   $('[name="tipo"]').val($(this).attr('id'));
    
});

</script>




@endsection

