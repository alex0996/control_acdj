@extends('layouts.home')

@section('content')

<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
                                <h4 class="card-title ">Gestión de Asistencias</h4>
                                <p class="card-category">Búsqueda por fecha</p>
                
        </div>
        <div class="card-body  mt-2">
 
                    <div class="form-row mt-4">
                            <div class="col-md-5">
                                <label for="fecha_busqueda">Fecha de búsqueda</label>
                              <input type="date" class="form-control" id="fecha_busqueda" name="fecha_busqueda">
                            </div>
                            <div class="col-md-5 offset-md-1">
                                <button class="btn btn-primary buscar">Buscar</button>
                            </div>
                    </div>
        
            
        </div>
    </div>
</div>

<script>

$('.buscar').click(function(){
    if($('#fecha_busqueda').val() == ""){
        alert('debe indicar una fecha');
    }
    else{
    window.location.href = "/editarAsistencia/"+$('#fecha_busqueda').val()+"";
    }
});

</script>

@endsection

