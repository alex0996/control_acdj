@extends('layouts.home')

@section('content')

<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
                <div class="row">
                        <div class="col-md-6">
                                <h4 class="card-title ">Gestión de Pensiones</h4>
                                <p class="card-category"> Detalle de pensiones</p>
                        </div>  
                        <div class="col-md-1 offset-md-5">
                        <button type="submit" id="nuevoItem" class="btn btn-white btn-round btn-just-icon">
                                <i class="material-icons">add</i>
                                <div class="ripple-container"></div>
                            </button>
                        </div>
                    </div>
       
        </div>
        <div class="card-body  mt-2">
            <div class="table-responsive">
                    
                <table class="table" id="pensionesTable">
                    <thead class=" text-primary">
                    <th>
                        #
                    </th>
                    <th>
                        Descripción
                    </th>
                    <th>
                        Opciones    
                    </th>
                    </thead>
                    <tbody id="informacionPensiones">
                            @if (count($pensiones) > 0)
                            @foreach ($pensiones as $pension)
                        <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$pension->descripcion}}</td>
                                    <td class="td-actions">
                                            
                                            <button type="button" rel="tooltip" title="Editar pension" class="btn btn-dark btn-link info" name="{{$pension->id}}">
                                              <i class="material-icons">edit</i>
                                            </button>
                                            <button type="button" rel="tooltip" title="Deshabilitar pension" class="btn btn-danger btn-link delete" name="{{$pension->id}}">
                                                    <i class="material-icons">close</i>
                                            </button>
                                    </td>
                        </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
      


            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" id="modal">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title">Información de la pensión</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form id="formPensiones">
                        <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" id="id" value="">
                        <div class="form-row">
                          <div class="col-md-10">
                              <label for="descripcion" class="bmd-label-floating">Descripción</label>
                            <input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Ejm: sin pensión" autocomplete="off" required>
                          </div>
                        </div>

                        <input type="submit" name="editarBtn" value="Editar" id="editarBtn" class="btn btn-primary mt-4"/> 
                        <input type="submit" name="agregarBtn" value="Agregar" id="agregarBtn" class="btn btn-success mt-4"/> 
                      
                    </form>
                    
                            
                       
        </div>
          </div>
        </div>
</div>



{{-- script de pensiones --}}
<script src="{{ asset('js/mantenimientos/pensiones.js') }}" defer></script>
 
@endsection

