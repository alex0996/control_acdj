@extends('layouts.home')

@section('content')

<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
                <div class="row">
                        <div class="col-md-6">
                                <h4 class="card-title ">Gestión de Asistencias</h4>
                                <p class="card-category"> 
                                        @foreach ($asistencias as $asistencia)
                                        @if ($loop->first) 
                                        Mostrando {{$asistencia->fecha}}
                                        @break
                                        @endif
                                    @endforeach
                                </p>
                        </div>  
                        <div class="col-md-1 offset-md-5">
                        <button type="submit" id="volver" class="btn btn-white btn-round btn-just-icon">
                                <i class="material-icons">arrow_back</i>
                                <div class="ripple-container"></div>
                            </button>
                        </div>
                    </div>
            
        </div>
        <div class="card-body  mt-2">
                
                <div class="table-responsive">

                        <table class="table" id="editar_asistencias">
                            <thead class=" text-primary">
                            <th>
                                #
                            </th>
                            
                            <th>
                                Cédula
                            </th>
                            <th>
                                Nombre
                            </th>
                            <th>
                                Asistió
                            </th>
                            <th>
                                Causa
                            </th>
                            <th>
                                Opciones
                            </th> 
                                      
                            </thead>
                            <tbody>
                                    @if (count($asistencias) > 0)
                                    @foreach ($asistencias as $asistencia)
                                    <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{\App\Http\Controllers\PersonasController::showFromAssistance($asistencia->id)->cedula}}</td>
                                            <td>{{\App\Http\Controllers\PersonasController::showFromAssistance($asistencia->id)->nombre}}</td>
                                            </td>
                                            @if ($asistencia->asistio == 1)
                                            <td><button type="button" class="btn btn-success btn-link">
                                                    <i class="material-icons">event_available</i>
                                            </button></td>
                                            <td>---</td>
                                            @else 
                                            <td><button type="button" class="btn btn-danger btn-link">
                                                    <i class="material-icons">event_busy</i>
                                            </button></td>
                                            <td>{{\App\Http\Controllers\CausasController::showFromAssistance($asistencia->causa)->motivo}}</td>
                                            @endif
                                            <td><button type="button" rel="tooltip" title="Editar asistencia" class="btn btn-dark btn-link edit" name="{{$asistencia->id}}">
                                                    <i class="material-icons">edit</i>
                                            </button></td>
                                        
                                    </tr>
                                    @endforeach
                                
                                @endif
                            </tbody>
                        </table>
        </div>
    </div>
</div>


<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" id="modal">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title">Información de asistencia</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form id="formAsistencias">
                        <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" id="id" value="">
                        <input type="hidden" name="fecha" id="fecha" value=@foreach ($asistencias as $asistencia) @if ($loop->first) {{$asistencia->fecha}}@break @endif @endforeach>
                        <div class="form-row">
                          <div class="col-md-5">
                              <label for="cedula" class="bmd-label-floating">Cédula</label>
                            <input type="text" disabled class="form-control" id="cedula" name="cedula" placeholder="Ejm: 116560494" autocomplete="off" >
                          </div>
                          <div class="col-md-5 offset-md-1">
                              <label for="nombre">Nombre</label>
                            <input type="text"  disabled class="form-control" id="nombre" name="nombre"  placeholder="Ejm: Juan Brenes Arias" autocomplete="off" >
                          </div>
                        </div>

                        <div class="form-row mt-4">
                                <div class="col-md-5">
                                        <label for="asistio">Asistió</label>
                                    <select class="form-control" id="asistio" name="asistio" required>
                                            <option value="" disabled selected>Click para mostrar opciones</option>
                                            <option id="1" value="1">Si</option>
                                            <option id="0" value="0">No</option>
                                    </select>
                                </div>
                                <div class="col-md-5 offset-md-1">
                                        <label for="tipo_pension">Causa</label>
                                        <select class="form-control" id="causa" name="causa" required>
                                                <option value="" id="def" disabled selected>Click para mostrar opciones</option>
                                                @foreach ($causas as $causa)
                                                <option id="{{$causa->id}}" value="{{$causa->id}}">{{$causa->motivo}}</option>
                                                @endforeach
                                        </select>
                                </div>
                        </div>

                        <input type="submit" name="editarBtn" value="Editar" id="editarBtn" class="btn btn-primary mt-4"/> 
                        
                    </form>
                       
        </div>
          </div>
        </div>
</div>


{{-- script de pensiones --}}
<script src="{{ asset('js/mantenimientos/asistencias.js') }}" defer></script> 


@endsection

