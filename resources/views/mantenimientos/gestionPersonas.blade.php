@extends('layouts.home')

@section('content')

<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
          <div class="row">
            <div class="col-md-6">
            <h4 class="card-title">Gestión de usuarios</h4>
            <p class="card-category"> Detalle de usuarios</p>
            </div>  
            <div class="col-md-1 offset-md-5">
            <button type="button" id="nuevoItem" class="btn btn-white btn-round btn-just-icon">
                    <i class="material-icons">add</i>
                    <div class="ripple-container"></div>
                </button>
            </div>
        </div>
        </div>
        <div class="card-body mt-2">
            <div class="table-responsive">
                <table class="table" id="personasTable">
                    <thead class=" text-primary">
                    <th>
                        #
                    </th>
                    <th>
                        Cédula
                    </th>
                    <th>
                        Nombre
                    </th>
                    <th>
                        Edad
                    </th>
                    <th>
                        Tipo de pensión
                    </th>
                    <th>
                        Opciones
                    </th>
                                
                    </thead>
                    <tbody id="informacionUsuarios">
                            @if (count($personas) > 0)
                            @foreach ($personas as $persona)
                        <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$persona->cedula}}</td>
                                    <td>{{$persona->nombre}}</td>
                                    <td>{{ date("Y") - intval($persona->fecha_nacimiento)}} años</td>
                                    <td>
                                    {{\App\Http\Controllers\PensionesController::showFromPeople($persona->tipo_pension)->descripcion}}
                                    </td>
                                    <td class="td-actions ">
                                            <button type="button" rel="tooltip" title="Ver más información" onclick="mostrarInformacion(1, {{$persona->id}})" class="btn btn-info btn-link info">
                                              <i class="material-icons">info</i>
                                            </button>
                                            <button type="button" rel="tooltip" title="Editar usuario" onclick="mostrarInformacion(0, {{$persona->id}})" class="btn btn-dark btn-link editar">
                                              <i class="material-icons">edit</i>
                                            </button>
                                            <button type="button" rel="tooltip" title="Deshabilitar usuario" class="btn btn-danger btn-link delete" name="{{$persona->id}}">
                                                    <i class="material-icons">close</i>
                                            </button>
                                    </td>
                            </tr>
                            @endforeach
                        
                        @endif
                    </tbody>
                </table>
      


            </div>
        </div>
    </div>
</div>


<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" id="modal">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title">Información del usuario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form id="formPersonas">
                        <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" id="id" value="">
                        <div class="form-row">
                          <div class="col-md-5">
                              <label for="cedula" class="bmd-label-floating">Cédula</label>
                            <input type="text" class="form-control" id="cedula" name="cedula" placeholder="Ejm: 116560494" autocomplete="off" required>
                          </div>
                          <div class="col-md-5 offset-md-1">
                              <label for="nombre">Nombre</label>
                            <input type="text" class="form-control" id="nombre" name="nombre"  placeholder="Ejm: Juan Brenes Arias" autocomplete="off" required>
                          </div>
                        </div>

                        <div class="form-row mt-4">
                                <div class="col-md-5">
                                    <label for="fecha_nacimiento">Fecha de nacimiento</label>
                                  <input type="date" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" placeholder="02/02/1990" required>
                                </div>
                                <div class="col-md-5 offset-md-1">
                                    <label for="sexo">Sexo</label>
                                    <select class="form-control" id="sexo" name="sexo" required>
                                            <option value="" disabled selected>Click para mostrar opciones</option>
                                            <option id="F" value="F">Femenino</option>
                                            <option id="M" value="M">Masculino</option>
                                    </select>
                                </div>
                        </div>

                        <div class="form-row mt-4">
                                <div class="col-md-5">
                                    <label for="tipo_pension">Tipo de pensión</label>
                                    <select class="form-control" id="tipo_pension" name="tipo_pension" required>
                                            <option value="" disabled selected>Click para mostrar opciones</option>
                                            @foreach ($pensiones as $pension)
                                            <option id="{{$pension->id}}" value="{{$pension->id}}">{{$pension->descripcion}}</option>
                                            @endforeach
                                    </select>
                                </div>
                                <div class="col-md-5 offset-md-1">
                                    <label for="monto_pension">Monto de pensión (₡)</label>
                                  <input type="number" class="form-control" id="monto_pension" name="monto_pension" placeholder="Monto en colones" autocomplete="off" required>
                                </div>
                        </div>

                        <div class="form-row mt-4">
                                <div class="col-md-5">
                                    <label for="transporte">Utiliza transporte</label>
                                    <select class="form-control" id="transporte" name="transporte" required>
                                            <option value="" disabled selected>Click para mostrar opciones</option>
                                            <option id="1" value="1">Si</option>
                                            <option id="0" value="0">No</option>
                                    </select>
                                </div>
                                <div class="col-md-5 offset-md-1">
                                    <label for="donacion">Monto de donación (₡)</label>
                                  <input type="number" class="form-control" id="donacion" name="donacion" placeholder="Monto en colones" autocomplete="off" required>
                                </div>
                        </div>

                        <input type="submit" name="editarBtn" value="Editar" id="editarBtn" class="btn btn-primary mt-4"/> 
                        <input type="submit" name="agregarBtn" value="Agregar" id="agregarBtn" class="btn btn-success mt-4"/> 
                        
                    </form>
                    
                            
                       
        </div>
          </div>
        </div>
</div>




{{-- script de usuarios --}}
<script src="{{ asset('js/mantenimientos/personas.js') }}" defer></script>
  

@endsection
