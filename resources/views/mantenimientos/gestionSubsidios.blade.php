@extends('layouts.home')

@section('content')


<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
                <div class="row">
                        <div class="col-md-3">
                                <h4 class="card-title ">Gestión de Subsidios</h4>
                                <p class="card-category"> Detalle de subsidios</p>
                        </div>  
                        <div class="col-md-1 offset-md-8">
                        <button type="submit" id="nuevoItem" class="btn btn-white btn-round btn-just-icon">
                                <i class="material-icons">add</i>
                                <div class="ripple-container"></div>
                            </button>
                        </div>
                    </div>
            
        </div>
        <div class="card-body  mt-2">
            <div class="table-responsive">
                <table class="table" id="subsidiosTable">
                    <thead class=" text-primary">
                    <th>
                        #
                    </th>
                    <th>
                        Entidad
                    </th>
                    <th>
                        Monto total
                    </th>
                    <th>
                        Usuarios totales
                    </th>
                    <th>
                        Monto por usuario
                    </th>
                    <th>
                        Opciones
                    </th>       
                    </thead>
                    <tbody id="informacionPensiones">
                            @if (count($subsidios) > 0)
                            @foreach ($subsidios as $subsidio)
                        <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$subsidio->entidad}}</td>
                                    <td>₡ {{$subsidio->monto_total}}</td>
                                    <td>{{$subsidio->cantidad_usuarios}}</td>
                                    <td>₡ {{$subsidio->monto_usuario}}</td>
                                    <td class="td-actions">
                                            <button type="button" rel="tooltip" title="Editar subsidio" class="btn btn-dark btn-link info" name="{{$subsidio->id}}">
                                              <i class="material-icons">edit</i>
                                            </button>
                                            <button type="button" rel="tooltip" title="Deshabilitar subsidio" class="btn btn-danger btn-link delete" name="{{$subsidio->id}}">
                                                    <i class="material-icons">close</i>
                                            </button>
                                    </td>
                        </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
      


            </div>
        </div>
    </div>
</div>



<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" id="modal">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title">Información del subsidio</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form id="formSubsidios">
                        <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" id="id" value="">
                        <div class="form-row">
                          <div class="col-md-5">
                              <label for="entidad" class="bmd-label-floating">Entidad</label>
                            <input type="text" class="form-control" id="entidad" name="entidad" placeholder="Ejm: JPS" autocomplete="off" required>
                          </div>
                          <div class="col-md-5 offset-md-1">
                              <label for="monto_total">Monto total (₡)</label>
                            <input type="number" class="form-control" id="monto_total" name="monto_total"  placeholder="Ejm: 1000000" autocomplete="off" required>
                          </div>
                        </div>

                        <div class="form-row mt-4">
                                <div class="col-md-5">
                                    <label for="cantidad_usuarios">Cantidad de usuarios</label>
                                  <input type="number" class="form-control" id="cantidad_usuarios" name="cantidad_usuarios" placeholder="Ejm: 15" autocomplete="off" required>
                                </div>
                        </div>

                        
                        <input type="submit" name="editarBtn" value="Editar" id="editarBtn" class="btn btn-primary mt-4"/> 
                        <input type="submit" name="agregarBtn" value="Agregar" id="agregarBtn" class="btn btn-success mt-4"/> 
                      
                    </form>
                    
                            
                       
        </div>
          </div>
        </div>
</div>



{{-- script de subsidios --}}
<script src="{{ asset('js/mantenimientos/subsidios.js') }}" defer></script>
  
 
@endsection
