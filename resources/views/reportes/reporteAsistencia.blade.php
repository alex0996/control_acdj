@extends('layouts.home')

@section('content')

<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">

                <div class="row">
                        <div class="col-md-6">
                                <h4 class="card-title ">Reporte de asistencia</h4>
                                <p class="card-category"> 
                                       Fecha: @foreach ($datos as $dato)
                            {{$dato}}
                            @endforeach
                                </p>
                        </div>  
                        <div class="col-md-1 offset-md-5">
                        <button type="submit" id="volver" class="btn btn-white btn-round btn-just-icon">
                                <i class="material-icons">arrow_back</i>
                                <div class="ripple-container"></div>
                            </button>
                        </div>
                    </div>

        </div>
        <div class="card-body mt-2">
            <div class="table-responsive">
                <table class="table display" id="reporteMesTable" style="width:100%">
                    <thead class=" text-primary">
                    <th>
                        #
                    </th>
                    <th>
                        Cédula
                    </th>
                    <th>
                        Nombre
                    </th>
                    <th>
                        Asistencia
                    </th>
                    <th>
                        Fecha
                    </th>
                                
                    </thead>
                    <tbody>
                            @if (count($personas) > 0)
                            @foreach ($personas as $persona)
                        <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$persona->cedula}}</td>
                                    <td>{{$persona->nombre}}</td>
                                    <td>{{$persona->asistio}} días</td>
                                    <td>@foreach ($datos as $dato)
                                            {{$dato}}
                                            @endforeach</td>
                            </tr>
                            @endforeach

                        @endif
                    </tbody>
                </table>
      


            </div>
        </div>
    </div>
</div>

<script>
$(document).ready( function () {
    $('#reporteMesTable').DataTable({
        "language": {
          "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        },
        dom: 'Bfrtip',
        buttons: [
    {
        text: 'PDF',
        extend: 'pdfHtml5',
        title: function () { return "Reporte de Asistencia - Asociación Centro Diurno Jerusalén" },
    message: '',
    exportOptions: {
         columns: ':visible'
    },
    customize: function (doc) {
        doc.pageMargins = [100,10,100,10];
        doc.defaultStyle.fontSize = 14;
        doc.styles.tableHeader.fontSize = 14;
        doc.styles.title.fontSize = 15;
        
        // Remove spaces around page title
        doc.content[0].text = doc.content[0].text.trim();
        
        // Styling the table: create style object
        var objLayout = {};
        // Horizontal line thickness
        objLayout['hLineWidth'] = function(i) { return .5; };
        // Vertikal line thickness
        objLayout['vLineWidth'] = function(i) { return .5; };
        // Horizontal line color
        objLayout['hLineColor'] = function(i) { return '#aaa'; };
        // Vertical line color
        objLayout['vLineColor'] = function(i) { return '#aaa'; };
        // Left padding of the cell
        objLayout['paddingLeft'] = function(i) { return 4; };
        // Right padding of the cell
        objLayout['paddingRight'] = function(i) { return 4; };
        // Inject the object in the document
        doc.content[1].layout = objLayout;
    }
    }
],
      });
} );


$('#volver').click(function(){
    window.location.href = "/reportes";
});

</script>

@endsection
