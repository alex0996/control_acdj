$(document).ready( function () {
    $('#control_asistencias').DataTable({
        "language": {
          "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        }
      });
} );


$('.absent').click(function () { 
  var id = $(this);
  var causa = $("select[name='"+$(this).attr('name')+"'"); 

  if(causa.find(":selected").attr('id') == undefined){
    swal("Error", "Debe seleccionar una causa", "error");
  } 
  else {
    var datos = "{\"id\": \""+id.attr('name')+"\", \"causa\": \""+causa.find(":selected").attr('id')+"\"}";
   
    $.ajax({
        type: "PUT",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: "/asistencias/"+id.attr('name')+"?_token="+$('#_token').val()+"",
        data: datos,
        success: function (response) {
          swal("Exito", "Los cambios se han aplicado correctamente", "success");
          //se deshabilitan las opciones.
          id.attr('disabled',true);
          causa.attr('disabled',true);
        },
        error: function(response){
          alert("no funco");
        }
      });

  }



});