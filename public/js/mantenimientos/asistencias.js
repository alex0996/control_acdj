$(document).ready( function () {
    $('#editar_asistencias').DataTable({
        "language": {
          "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        }
      });
} );


$('.edit').click(function(){

    var datos = "{\"fecha\": \""+$('#fecha').val()+"\"}";
    var nombre = $(this).closest('tr').find('td:eq(2)').text();
    var cedula = $(this).closest('tr').find('td:eq(1)').text();

    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: "/asistencias/"+$(this).attr('name')+"",
        data: JSON.parse(datos),
        success: function (response) {
            $('#id').val(response[0].id);
            if(response[0].asistio == 1){$("#asistio option[value='"+response[0].asistio+"'").prop('selected', true);
            $('#causa').attr("disabled", true);}
            else{$("#asistio option[value='"+response[0].asistio+"'").prop('selected', true);}
            $("#causa option[value='"+response[0].causa+"'").prop('selected', true);
            $('#cedula').val(cedula);
            $('#nombre').val(nombre);
            $('#fecha').val($('#fecha').val());
            $('#modal').modal('show');   
        }
    });

});

$('#asistio').change(function(){

    if($(this).find(":selected").attr('id') == "1"){
        $('#causa').attr("disabled", true);$("#causa option[value='5'").prop('selected', true);
    }
    else if($(this).find(":selected").attr('id') == "0"){
        $('#causa').attr("disabled", false);$("#causa option[id='def'").prop('selected', true);
    } 
});



$('#volver').click(function(){
    window.location.href = "/buscarAsistencia";
});

$('#formAsistencias').submit(function(event) {
   
        $('#causa').attr("disabled", false);
        var datos = JSON.stringify($('#formAsistencias').serializeObject());
        event.preventDefault();

        $.ajax({
            type: "PUT",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: "/editarAsistencia/"+$('#id').val()+"?_token="+$('#_token').val()+"",
            data: datos,
            success: function (response) {
                swal("Exito", "Los cambios se han aplicado correctamente", "success").then(function(){
                    location.reload();
                });
            },
            error: function(){
                swal("Error", "No se pudo realizar la operación", "error");
            }
        });
    

});




