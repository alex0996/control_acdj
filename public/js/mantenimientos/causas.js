$(document).ready( function () {
    $('#causasTable').DataTable({
        "language": {
          "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        }
      });
} );

$('#nuevoItem').click(function () { 
    $("#formCausas :input").attr("disabled", false); $('#formCausas').trigger("reset");
    $('#editarBtn').addClass("collapse");
    $('#agregarBtn').removeClass("collapse");
    $('#id').val('');
    $('#modal').modal('show');
});

//mostrar informacion

$('.info').click(function () { 
    
    $('#editarBtn').removeClass("collapse"); $('#agregarBtn').addClass("collapse");

    $.ajax({
        type: "GET",
        url: "/causas/"+ $(this).attr('name') +"",
        success: function (response) {
            $('#id').val(response.id);
            $('#motivo').val(response.motivo);
            $('#modal').modal('show');
        },
        error: function(){
            swal("Error", "No se pudo realizar la operación", "error");
        }
    });
 
  
});

$('.delete').click(function () { 
    
    swal({
        title: "Se eliminará una causa",
        text: "Los cambios no se podran revertir",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: "DELETE",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    url: "/causas/"+$(this).attr('name')+"?_token="+$('#_token').val()+"",
                    success: function (response) {
                        swal("Se ha eliminado la causa", {
                            icon: "success",
                        }).then(function(){
                            location.reload();
                        });
                    },
                    error: function(){
                        swal("Error", "No se pudo realizar la operación", "error");
                    }
                });
               
            } else {
                swal("No se realizo ningun cambio");
            }
        });
   



});

$('#formCausas').submit(function(event) {
    var datos = JSON.stringify($('#formCausas').serializeObject());
    event.preventDefault();
 
    if(!$('#editarBtn').hasClass("collapse")){
        $.ajax({
            type: "PUT",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: "/causas/"+$('#id').val()+"?_token="+$('#_token').val()+"",
            data: datos,
            success: function (response) {
                swal("Exito", "Los cambios se han aplicado correctamente", "success").then(function(){
                    location.reload();
                });
            },
            error: function(){
                swal("Error", "No se pudo realizar la operación", "error");
            }
        });
    } else {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: "/causas?_token="+$('#_token').val()+"",
            data: datos,
            success: function (response) {
                swal("Exito", "Se ha agregado la causa", "success").then(function(){
                    location.reload();
                });
            },
            error: function(){
                swal("Error", "No se pudo realizar la operación", "error");
            }
        });
    }


});