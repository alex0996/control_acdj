$(document).ready( function () {
    $('#personasTable').DataTable({
        "language": {
          "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        }
      });
} );

$('#nuevoItem').click(function () { 
    $("#formPersonas :input").attr("disabled", false); $('#formPersonas').trigger("reset");
    $('#editarBtn').addClass("collapse");
    $('#agregarBtn').removeClass("collapse");
    $('#id').val('');
    $('#modal').modal('show');
});

//mostrar informacion
function mostrarInformacion(estado, id){

    if(estado == 1){$("#formPersonas :input").attr("disabled", true);$('#editarBtn').addClass("collapse");$('#agregarBtn').addClass("collapse");}
    else{$("#formPersonas :input").attr("disabled", false);$('#editarBtn').removeClass("collapse");$('#agregarBtn').addClass("collapse");}

    $.ajax({
        type: "GET",
        url: "/personas/"+ id +"",
        success: function (response) {
            $('#id').val(response.id);
            $('#cedula').val(response.cedula);
            $('#nombre').val(response.nombre);
            $('#fecha_nacimiento').val(response.fecha_nacimiento);
            $("#sexo option[value="+response.sexo+"").prop('selected', true);
            $("#tipo_pension option[value='"+response.tipo_pension+"'").prop('selected', true);
            $('#monto_pension').val(response.monto_pension);
            $("#transporte option[value='"+response.transporte+"'").prop('selected', true);
            $('#donacion').val(response.donacion);
            $('#modal').modal('show');
        },
        error: function(){
            swal("Error", "No se pudo realizar la operación", "error");
        }
    });
 

}

$('.delete').click(function () { 
    swal({
        title: "Se eliminará una persona",
        text: "Los cambios no se podran revertir",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: "DELETE",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    url: "/personas/"+$(this).attr('name')+"?_token="+$('#_token').val()+"",
                    success: function (response) {
                        swal("Se ha eliminado la persona", {
                            icon: "success",
                        }).then(function(){
                            location.reload();
                        });
                    },
                    error: function(){
                        swal("Error", "No se pudo realizar la operación", "error");
                    }
                });
               
            } else {
                swal("No se realizo ningun cambio");
            }
        });
   
    
});

$('#formPersonas').submit(function(event) {
    
    var datos = JSON.stringify($('#formPersonas').serializeObject());
    event.preventDefault();

    if(!$('#editarBtn').hasClass("collapse")){

        $.ajax({
            type: "PUT",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: "/personas/"+$('#id').val()+"?_token="+$('#_token').val()+"",
            data: datos,
            success: function (response) {
                swal("Exito", "Los cambios se han aplicado correctamente", "success").then(function(){
                    location.reload();
                });
            },
            error: function(){
                swal("Error", "No se pudo realizar la operación", "error");
            }
        });
    
    } 
    else {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: "/personas?_token="+$('#_token').val()+"",
            data: datos,
            success: function (response) {
                swal("Exito", "Se ha agregado la persona", "success").then(function(){
                    location.reload();
                });
            },
            error: function(){
                swal("Error", "No se pudo realizar la operación", "error");
            }
        });
    }


  
});