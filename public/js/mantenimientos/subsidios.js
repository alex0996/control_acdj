$(document).ready( function () {
    $('#subsidiosTable').DataTable({
        "language": {
          "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        }
      });
} );

$('#nuevoItem').click(function () { 
    $("#formSubsidios :input").attr("disabled", false); $('#formSubsidios').trigger("reset");
    $('#editarBtn').addClass("collapse");
    $('#agregarBtn').removeClass("collapse");
    $('#id').val('');
    $('#modal').modal('show');
});


//mostrar informacion

$('.info').click(function () { 
    
    $('#editarBtn').removeClass("collapse");$('#agregarBtn').addClass("collapse");

    $.ajax({
        type: "GET",
        url: "/subsidios/"+ $(this).attr('name') +"",
        success: function (response) {
            $('#id').val(response.id);
            $('#entidad').val(response.entidad);
            $('#monto_total').val(response.monto_total);
            $('#cantidad_usuarios').val(response.cantidad_usuarios);

            
            $('#modal').modal('show');
        },
        error: function(){
            swal("Error", "No se pudo realizar la operación", "error");
        }
    });
 
  
});

$('.delete').click(function () { 
    
    swal({
        title: "Se eliminará un subsidio",
        text: "Los cambios no se podran revertir",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: "DELETE",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    url: "/subsidios/"+$(this).attr('name')+"?_token="+$('#_token').val()+"",
                    success: function (response) {
                        swal("Se ha eliminado el subsidio", {
                            icon: "success",
                        }).then(function(){
                            location.reload();
                        });
                    },
                    error: function(){
                        swal("Error", "No se pudo realizar la operación", "error");
                    }
                });
               
            } else {
                swal("No se realizo ningun cambio");
            }
        });

    
});

$('#formSubsidios').submit(function(event) {
    var datos = JSON.stringify($('#formSubsidios').serializeObject());
    event.preventDefault();
 
    if(!$('#editarBtn').hasClass("collapse")){
        $.ajax({
            type: "PUT",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: "/subsidios/"+$('#id').val()+"?_token="+$('#_token').val()+"",
            data: datos,
            success: function (response) {
                swal("Exito", "Los cambios se han aplicado correctamente", "success").then(function(){
                    location.reload();
                });
            },
            error: function(){
                swal("Error", "No se pudo realizar la operación", "error");
            }
        });
    } else {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: "/subsidios?_token="+$('#_token').val()+"",
            data: datos,
            success: function (response) {
                swal("Exito", "Se ha agregado el subsidio", "success").then(function(){
                    location.reload();
                });
            },
            error: function(){
                swal("Error", "No se pudo realizar la operación", "error");
            }
        });
    }

 


});