$(document).ready( function () {
    $('#pensionesTable').DataTable({
        "language": {
          "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        }
      });
} );

$('#nuevoItem').click(function () { 
    $("#formPensiones :input").attr("disabled", false); $('#formPensiones').trigger("reset");
    $('#editarBtn').addClass("collapse");
    $('#agregarBtn').removeClass("collapse");
    $('#id').val('');
    $('#modal').modal('show');
});

//mostrar informacion

$('.info').click(function () { 
    
    $('#editarBtn').removeClass("collapse"); $('#agregarBtn').addClass("collapse");

    $.ajax({
        type: "GET",
        url: "/pensiones/"+ $(this).attr('name') +"",
        success: function (response) {
            $('#id').val(response.id);
            $('#descripcion').val(response.descripcion);
            $('#modal').modal('show');
        },
        error: function(){
            swal("Error", "No se pudo realizar la operación", "error");
        }
    });
 
  
});

$('.delete').click(function () { 
    
    swal({
        title: "Se eliminará un tipo de pensión",
        text: "Los cambios no se podran revertir",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: "DELETE",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    url: "/pensiones/"+$(this).attr('name')+"?_token="+$('#_token').val()+"",
                    success: function (response) {
                        swal("Se ha eliminado la pensión", {
                            icon: "success",
                        }).then(function(){
                            location.reload();
                        });
                    },
                    error: function(){
                        swal("Error", "No se pudo realizar la operación", "error");
                    }
                });
               
            } else {
                swal("No se realizo ningun cambio");
            }
        });
  
});

$('#formPensiones').submit(function(event) {
   
    var datos = JSON.stringify($('#formPensiones').serializeObject());
    event.preventDefault();
 
    if(!$('#editarBtn').hasClass("collapse")){
        
        $.ajax({
            type: "PUT",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: "/pensiones/"+$('#id').val()+"?_token="+$('#_token').val()+"",
            data: datos,
            success: function (response) {
                swal("Exito", "Los cambios se han aplicado correctamente", "success").then(function(){
                    location.reload();
                });
            },
            error: function(){
                swal("Error", "No se pudo realizar la operación", "error");
            }
        });

    } else {
        
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: "/pensiones?_token="+$('#_token').val()+"",
            data: datos,
            success: function (response) {
                swal("Exito", "Se ha agregado la pensión", "success").then(function(){
                    location.reload();
                });
            },
            error: function(){
                swal("Error", "No se pudo realizar la operación", "error");
            }
        });
    }


});