<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//autenticacion
Auth::routes();

Route::get('/', function () {
    return view('auth.login');
})->middleware('guest');

//home de la aplicacion
Route::get('/home', 'HomeController@index')->middleware('auth');

//busqueda de asistencia para editar.
Route::get('/buscarAsistencia', function () {
    return view('control.busquedaAsistencia');
})->middleware('auth');

//endpoint reportes
Route::resource('/reportes', 'ReportesController')->middleware('auth');

//endpoint personas
Route::resource('/personas','PersonasController')->middleware('auth');

//endpoint asistencias
Route::resource('/asistencias','AsistenciasController')->middleware('auth');

//endpoint subsidio
Route::resource('/subsidios','SubsidiosController')->middleware('auth');

//endpoint tipo pension
Route::resource('/pensiones','PensionesController')->middleware('auth');

//endpoint causas
Route::resource('/causas','CausasController')->middleware('auth');

//endpoint editar asistencias
Route::resource('/editarAsistencia','EditarAsistenciasController')->middleware('auth');

